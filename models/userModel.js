
const mongoose = require("mongoose");
const bcrypt = require("bcrypt");

var userSchema = new mongoose.Schema({
    firstname: {
      type: String,
      required: true,
    },
    lastname: {
      type: String,
      required: true,
    },
    email: {
        type: String,
        required: true,
    },
    mobile: {
      type: String,
      required: true,
      unique: true,
    },
    password: {
        type: String,
        required: true,
    },
    isAdmin: {
        type: Boolean,
        default: false,
    },
    role: {
      type: String,
      default: "user",
    },
//=======================User Nested Array ======================   

    order: [
            {
                products: [ 
                    {
                        _id: false,
                        productName:{
                            type:String,
                            required: [true, "ProductName is required."],   
                        },
                        productQuantity: {
                            type:Number,
                            required: [true, "Quantity is required."],
                        },
                    
                    },
                        ],

                totalAmount: {
                    type: Number,
                    required: [true, "Total Amount is required."], 
                },
                purchasedOn: {
                    type: Date,
                    default: new Date(),
                },

            },

    ]

//=================================================================

});

userSchema.pre("save", async function (next) {
  const salt = await bcrypt.genSaltSync(10);
  this.password = await bcrypt.hash(this.password, salt);
});
userSchema.methods.isPasswordMatched = async function (enteredPassword) {
  return await bcrypt.compare(enteredPassword, this.password);
};
 
//Export the model
module.exports = mongoose.model("User", userSchema);
