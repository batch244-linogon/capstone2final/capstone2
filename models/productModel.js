const mongoose = require("mongoose");

var productSchema = new mongoose.Schema(
    {
        name: {
            type: String,
            required: [ true, "Product name is required."]
        },
        description: {
            type: String,
            required: [true, "Description is required."]
        },
        price: {
            type: Number,
            required: [true, "Price is required."]
        },
        slug: {
            type: String,
            required: true,
            unique: true,
            lowercase: true,
            default: true,
        },
        isActive: {
            type: Boolean,
            default: true,
        },
        createdOn: {
            type: Date,
            default: new Date(),
        },
        
        
        orders : [
            {
                orderId: {
                    type: String,
                    unique: true,
                    required: [true, "orderID is required."]
                }

            }
        ],

    }
)


module.exports = mongoose.model("Product", productSchema);