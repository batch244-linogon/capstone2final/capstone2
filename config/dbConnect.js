const { default: mongoose } = require("mongoose");

const dbConnect = () => {
  try {
    //const conn = mongoose.connect("mongodb+srv://admin:admin@zuitt-course-booking.g2pnldk.mongodb.net/Eshop?retryWrites=true&w=majority");
    const conn = mongoose.connect(process.env.MONGODB_URL);
    console.log("Database Connected Successfully");
  } catch (error) {
    console.log("DAtabase error");
  }
};
module.exports = dbConnect;