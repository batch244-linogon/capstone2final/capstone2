const User = require("../models/userModel");
const asyncHandler = require("express-async-handler");
const { generateToken } = require("../config/jwtToken");
const jwt = require("jsonwebtoken");
const { generateRefreshToken } = require("../config/refreshtoken");
const uniqid = require("uniqid");
const validateMongoDbId = require("../utils/validateMongodbId");


// Create a User ----------------------------------------------
const createUser = asyncHandler(async (req, res) => { 
const email = req.body.email;  
const findUser = await User.findOne({ email: email });
  if (!findUser) {  
    //Create a new user  
    const newUser = await User.create(req.body);
    res.json(newUser);
  } else {   
    throw new Error("User Already Exists");
  }
});

// Login a user =================================================
const loginUserCtrl = asyncHandler(async (req, res) => {
  const { email, password } = req.body;
  // check if user exists or not
  const findUser = await User.findOne({ email });
  if (findUser && (await findUser.isPasswordMatched(password))) {
    const refreshToken = await generateRefreshToken(findUser?._id);
    const updateuser = await User.findByIdAndUpdate(
      findUser.id,
      {
        refreshToken: refreshToken,
      },
      { new: true }
    );
    res.cookie("refreshToken", refreshToken, {
      httpOnly: true,
      maxAge: 72 * 60 * 60 * 1000,
    });
    res.json({
      _id: findUser?._id,
      firstname: findUser?.firstname,
      lastname: findUser?.lastname,
      email: findUser?.email,
      mobile: findUser?.mobile,
      token: generateToken(findUser?._id),
    });
  } else {
    throw new Error("Invalid Credentials");
  }
});

// Get all users ========================================
const getallUser = asyncHandler(async (req, res) => {
  try {
    const getUsers = await User.find().populate("email");
    res.json(getUsers);
  } catch (error) {
    throw new Error(error);
  }
});

// Get a single user ====================================

const getaUser = asyncHandler(async (req, res) => {
  //console.log(req.params);
  const { id } = req.params;
  validateMongoDbId(id);
  try {
    const getaUser = await User.findById(id);
    res.json({
      getaUser,
    });
  } catch (error) {
    throw new Error(error);
  }
});

// DELETE a single user =================================
const deleteaUser = asyncHandler(async (req, res) => {
  const { id } = req.params;
  validateMongoDbId(id);

  try {
    const deleteaUser = await User.findByIdAndDelete(id);
    res.json({
      deleteaUser,
    });
  } catch (error) {
    throw new Error(error);
  }
});

// Update a user =========================================
const updatedUser = asyncHandler(async (req, res) => {
  //const { _id } = req.user;
  const { id } = req.params;
  //validateMongoDbId(_id);

  try {
    const updatedUser = await User.findByIdAndUpdate(
      id,
      {
        firstname: req?.body?.firstname,
        lastname: req?.body?.lastname,
        email: req?.body?.email,
        mobile: req?.body?.mobile,
      },
      {
        new: true,
      }
    );
    res.json(updatedUser);
  } catch (error) {
    throw new Error(error);
  }
});

module.exports = { createUser, loginUserCtrl, getallUser, getaUser, deleteaUser, updatedUser, };